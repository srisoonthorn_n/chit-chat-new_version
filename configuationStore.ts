// import AsyncStorage from "@react-native-community/async-storage";
// import { createStore } from "redux";
// import { persistStore, persistReducer } from "redux-persist";
// import storage from "redux-persist/lib/storage";
// import { rootReducer, RootState } from "./redux";

// const persistConfig = {
//   key: "root",
//   storage: AsyncStorage,
//   blacklist: ["CasesReducer"]
// };

// const persistedReducer = persistReducer(persistConfig, rootReducer);

// let store = createStore(persistedReducer);
// let persistor = persistStore(store);
// export { store, persistor };

// Imports: Dependencies
import AsyncStorage from "@react-native-community/async-storage";
import { createStore, applyMiddleware } from "redux";
import { persistStore, persistReducer } from "redux-persist";
import { rootReducer } from "./redux";

// Imports: Redux
// import rootReducer from "../reducers/index";
// Middleware: Redux Persist Config
const persistConfig = {
  // Root
  key: "root",
  // Storage Method (React Native)
  storage: AsyncStorage,
  // Whitelist (Save Specific Reducers)
  // whitelist: ["casesReducer"],
  // Blacklist (Don't Save Specific Reducers)

};
// Middleware: Redux Persist Persisted Reducer
const persistedReducer = persistReducer(persistConfig, rootReducer);
// Redux: Store
const store = createStore(persistedReducer);
// Middleware: Redux Persist Persister
let persistor = persistStore(store);
// Exports
export { store, persistor };
