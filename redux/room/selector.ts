import { RootState } from "..";


export const getRoomState = (state: RootState) => {
  return state.room.rooms;
};

// export const getCase = (state: RootState)  => (id: string) : Case=> {
//   const caseInfo = state.cases.cases.find(m => m.id == id) as Case

//   return caseInfo ;
// };
