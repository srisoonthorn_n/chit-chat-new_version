import { RoomsState ,RoomsActionInterface ,RoomsActionType, Rooms, ListUsers} from "./types";

const initState: RoomsState = {
    rooms: [],
    ListUser :[],
  };

  export function addNewRoom(item: Rooms): RoomsActionInterface {
    return {
      type: RoomsActionType.ADD_NEW_ROOM,
      payload: item,
    };
  }

  export function SetRooms(rooms: Rooms[]): RoomsActionInterface {
    return {
      type: RoomsActionType.SET_ROOMS,
      payload: {
        rooms,
      },
    };
  }

  export function SetListUser(ListUser: ListUsers[]): RoomsActionInterface {
    return {
      type: RoomsActionType.LIST_USERS,
      payload: {
        ListUser,
      },
    };
  }
  
export const RoomsReducer = (
    state = initState,
    action: RoomsActionInterface
  ): RoomsState => {
    switch (action.type) {
      case RoomsActionType.ADD_NEW_ROOM:
        return {...state, rooms: [action.payload, ...state.rooms] };
        case RoomsActionType.SET_ROOMS:
          return {
            ...state,
            rooms: [...action.payload.rooms],
          };
          case RoomsActionType.LIST_USERS:
            return{
              ...state,
              ListUser : [...action.payload.ListUser]
            }
      default:
        return state;
    }
  };
  