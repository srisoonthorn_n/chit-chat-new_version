export enum RoomsActionType {
    ADD_NEW_ROOM = "ADD_NEW_ROOM",
    SET_ROOMS = "SET_ROOMS",
    LIST_USERS = "LIST_USERS"
  }


export type RoomsAction = RoomsActionType;

export interface RoomsActionInterface {
    type: RoomsAction;
    payload: any;
  }

  
export interface Rooms {
    key : string;
    id : string;
    nameRoom : string;
    password : string;
    status : string;
    admin : string;
    timeToCreate : string;
  }

  export interface RoomsState {
    rooms: Rooms[];
    ListUser: ListUsers[];
  }

  export interface ListUsers {
    name : string,
    status : string,
  }
