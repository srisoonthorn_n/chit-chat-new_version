import { combineReducers } from "redux";
import { AppReducer } from "./app/appReducer";
import { AuthReducer } from "./auth/authReducer";
import { RoomsReducer } from "./room/roomReducer";

export const rootReducer = combineReducers({

  app: AppReducer,
  auth : AuthReducer,
  room : RoomsReducer
});

export type RootState = ReturnType<typeof rootReducer>;
