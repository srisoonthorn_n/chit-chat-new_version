import { AppActionInterface, AppActionType, AppState } from "./type";

const initState: AppState = {
  currentActiveBottomTab: "",
  previousActiveBottomTab: "",
  openBottomSheet : false,
};

export function SetCurrentActiveBottomTab(tabName: string): AppActionInterface {
  return {
    type: AppActionType.SET_CURRENT_ACTIVE_BOTTOM_TAB,
    payload: tabName,
  };
}

export function SetBottomSheet(bottomSheet: boolean): AppActionInterface {
  return {
    type: AppActionType.SET_OPEN_BOTTOM_SHEET,
    payload: bottomSheet,
  };
}


export const AppReducer = (
  state = initState,
  action: AppActionInterface
): AppState => {
  switch (action.type) {
    case AppActionType.SET_CURRENT_ACTIVE_BOTTOM_TAB:
      return {
        ...state,
        previousActiveBottomTab: state.currentActiveBottomTab,
        currentActiveBottomTab: action.payload,
      };
      case AppActionType.SET_OPEN_BOTTOM_SHEET :
        return {
          ...state,
          openBottomSheet : action.payload
        }
    default:
      return state;
  }
};
