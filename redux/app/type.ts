

export enum AppActionType {
  SET_CURRENT_ACTIVE_BOTTOM_TAB = "SET_CURRENT_ACTIVE_BOTTOM_TAB",
  SET_OPEN_BOTTOM_SHEET = "SET_OPEN_BOTTOM_SHEET"
}

export type AppAction = AppActionType;

export interface AppActionInterface {
  type: AppAction;
  payload: any;
}

export interface AppState {
  currentActiveBottomTab: string;
  previousActiveBottomTab: string;
  openBottomSheet : boolean;
}
