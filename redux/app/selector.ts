import { RootState } from "..";


export const getCurrentBottomTab = (state: RootState) => {
  return state.app.currentActiveBottomTab;
};
