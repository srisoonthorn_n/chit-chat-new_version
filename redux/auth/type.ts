export enum UserActionType {
    LOGIN = "LOGIN",
    LOGOUT = "LOGOUT",
  }
  
  export type UserAction = UserActionType;
  
  export interface UserActionInterface {
    type: UserAction;
    payload: any;
  }
  
  export interface AuthState {
    isLogin: boolean | false;
    username: string;
    token: string;
    keyData : string;
  }
  