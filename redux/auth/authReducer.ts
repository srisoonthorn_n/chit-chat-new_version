import { AuthState, UserActionInterface, UserActionType } from "./type";


const initState: AuthState = {
  isLogin: false,
  username: "",
  token: "",
  keyData : "",
};

export function loginSuccess(
  token: string,
  username: string,
  keyData : string,
): UserActionInterface {
  return {
    type: UserActionType.LOGIN,
    payload: { token, username,keyData },
  };
}

export function logout(): UserActionInterface {
  return {
    type: UserActionType.LOGOUT,
    payload: initState,
  };
}

export const AuthReducer = (
  state = initState,
  action: UserActionInterface
): AuthState => {
  switch (action.type) {
    case UserActionType.LOGIN:
      return {
        ...state,
        isLogin: true,
        token: action.payload.token,
        username: action.payload.username,
        keyData : action.payload.keyData
      };
    case UserActionType.LOGOUT:
      return initState;
    default:
      return state;
  }
};
