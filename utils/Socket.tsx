import io from 'socket.io-client';
class SocketClass {

    constructor() {
        this.socket = null;
    }
    connect(endpoint) {
        this.socket = io(endpoint)
        // console.log(this.socket)
    }
    disconnect() {
        if (this.socket && this.socket.connected)
            this.socket.disconnect();
    }
    emit({ event, data }) {
        if (this.socket && this.socket.connected)
            this.socket.emit(event, data)
    }

    on({ event, fn }) {
        if (this.socket && this.socket.connected)
            return this.socket.on(event, fn);
    }

    off({ event, fn }) {
        if (this.socket && this.socket.connected)
            return this.socket.off(event, fn)
    }
}

const SocketApi = SocketClass.prototype
export default SocketApi;

