import { useNavigation } from '@react-navigation/native';
import React, { useEffect, useState } from 'react';
import { Dimensions, Image, KeyboardAvoidingView, Platform, StyleSheet } from 'react-native';
import { useDispatch, useSelector } from 'react-redux';
import Button from '../components/Button';
import TextInput from '../components/TextInput';
import { Text, View } from '../components/Themed';
import { loginSuccess } from '../redux/auth/authReducer';
import uuid4 from 'react-native-uuid'
import { RootState } from '../redux';
import { rootRef } from '../firebase/firebase';
const { width, height } = Dimensions.get("window");
const versionApp = require("../package.json");


export default function LoginScreen() {
    const navigation = useNavigation();
    const dispatch = useDispatch();
    const [username, setUsername] = useState("");
    const [password, setPassword] = useState("");

    const isLogin = useSelector((state: RootState) => state.auth.isLogin);

    useEffect(() => {
        if (isLogin) {
            navigation.navigate("Main");
        }
    }, []);

    const onLogin = async () => {
        var uuid = uuid4();
        var countData = uuid4();
        await rootRef.ref('users/' + countData).set({ name: username })
        .then(() => {
                dispatch(loginSuccess(uuid, username, countData));
                navigation.navigate("Main");
            }).catch((error: any) => {
                console.log(error)
            });
    }

    return (
        <View style={styles.container}>
            <KeyboardAvoidingView
                behavior={Platform.OS === "ios" ? "position" : "position"}
                keyboardVerticalOffset={50}
            >
                <View style={{
                    backgroundColor: "transparent",
                    width: "100%",
                    height: 322,
                    alignItems: "center",
                    justifyContent: "center",
                    position: "relative",
                    top: 50,
                    zIndex: 999
                }}>
                    <Image
                        source={require("../assets/images/chitchat_logo.png")}
                        style={{
                            height: Platform.OS === "ios" ? "100%" : "100%",
                            width: "100%",
                            resizeMode: "contain",
                        }}
                    />
                </View>

                <View style={{ marginHorizontal: width > 320 ? 57 : 20, borderRadius: 10, justifyContent: "center", backgroundColor: "#FFFFFF", }}>
                    <View style={{ paddingHorizontal: 40, paddingVertical: 47, borderRadius: 10, backgroundColor: "transparent", }}>
                        <View style={{ marginBottom: 16, backgroundColor: "transparent", }}>
                            <Text style={{ color: username ? "#F27579" : "#8D8586", fontSize: 14 }}>Username</Text>
                            <TextInput placeholder='Username'
                                style={{ height: 32, backgroundColor: "transparent", }}
                                onChangeText={(e: any) => {
                                    setUsername(e)
                                }} />
                        </View>

                        <View style={{ backgroundColor: "transparent", }}>
                            <Text style={{ color: password ? "#F27579" : "#8D8586", fontSize: 14 }}>Password</Text>
                            <TextInput placeholder='Password'
                                style={{ height: 32, backgroundColor: "transparent", }}
                                secureTextEntry
                                onChangeText={(e: any) => {
                                    setPassword(e)
                                }} />
                        </View>

                    </View>

                </View>

                <View style={{
                    alignItems: "center",
                    justifyContent: "center",
                    marginHorizontal: 55,
                    backgroundColor: "transparent",
                    marginTop: 40,

                }}>
                    <Button
                        login
                        title="Login"
                        onPress={() => {
                            onLogin()

                        }}
                    />
                </View>
            </KeyboardAvoidingView>
            <View style={{ backgroundColor: "transparent", bottom: 5, position: "absolute", marginStart: 8, }}>
                <Text style={{ fontSize: 14, color: "#FFFFFF" }}>{"V." + versionApp.version}</Text>
            </View>

        </View >
    );
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        // alignItems: 'center',
        justifyContent: width <= 1000 ? undefined : 'center',
        backgroundColor: "#F27579",
        paddingBottom: width <= 375 ? 50 : 0
    },

});