import React, { createRef, useEffect, useRef, useState } from 'react';
import { Alert, Image, KeyboardAvoidingView, Platform, ScrollView, StyleSheet, TextInput, TouchableOpacity } from 'react-native';
import { Text, View } from '../components/Themed';
import { AntDesign } from '@expo/vector-icons';
import { FontAwesome } from '@expo/vector-icons';
// import BottomSheet from 'react-native-js-bottom-sheet'
import { useDispatch, useSelector } from 'react-redux';
import { RootState } from '../redux';
import { SetBottomSheet } from '../redux/app/appReducer';
import Animated from 'react-native-reanimated';
import BottomBar from '../components/BottomBar';
import { Avatar } from 'react-native-elements';
import { getRoomState } from '../redux/room/selector';
import SocketApi from '../utils/Socket';
import { io } from 'socket.io-client';
import moment from 'moment';
import { useNavigation } from '@react-navigation/native';
import Messages from '../components/Messages';
import { SetListUser } from '../redux/room/roomReducer';
import Constants from 'expo-constants';
import * as Permissions from "expo-permissions";
import * as ImagePicker from 'expo-image-picker';
import uuid4 from 'react-native-uuid'
import { storage } from '../firebase/firebase';
import { Colors, ProgressBar } from 'react-native-paper';



// let EndPoint = "http://192.168.1.27:9000";

let EndPoint = "http://10.80.14.192:9000";


let socket: any

var scrollView: any

const firebase = require("firebase")
require("firebase/firestore")

export default function ChatScreen({ route }: any) {

    let fall = new Animated.Value(1);
    const dispatch = useDispatch();
    const navigation = useNavigation();

    // console.log("route ", route)

    const openBottomSheet = useSelector((state: RootState) => state.app.openBottomSheet)
    const userName = useSelector((state: RootState) => state.auth.username)
    const nameRoom = route.params.params.item.nameRoom

    const [mgs, setMge] = useState("")
    const [messages, setMessages] = useState([]);

    const [image, setImage] = useState<string>("");
    const [urlImage, setUrlImage] = useState("");
    const [imageName, setImageName] = useState("");
    const [progressBar, setProgressBar] = useState(
        {
            status: false,
            progress: 0
        })


    // console.log("userName", userName)
    // console.log("messages", messages)

    useEffect(() => {
        socket = io(EndPoint, {
            secure: true,
            transports: ['websocket'],
        });

        socket.emit('join', { nameRoom: nameRoom, name: userName }, () => {
        });

        socket.on('message', (mgs: any) => {
            setMessages((allmessagesTemp): any => [...allmessagesTemp, mgs])
        })


        socket.on('roomUser', ({ users, nameRoom }) => {
            // console.log("user", users)
            dispatch(SetListUser(users))
        })

        return () => {
            socket.emit('disconnected')
            socket.close();
            // socket.off();
            // navigation.navigate("Main")
        }

    }, [])

    useEffect(() => {
        getPhotoPermission()
    }, [])

    const getPhotoPermission = async () => {
        if (Constants.platform?.ios) {
            const { status } = await Permissions.askAsync(Permissions.CAMERA_ROLL)
            if (status != "granted") {
                alert("We nedd permission to access your camera roll")
            }
        } else {
            const { status } = await Permissions.askAsync(Permissions.CAMERA);
            if (status != "granted") {
                alert("We nedd permission to access your camera roll")
            }
        }
    }

    // const pickImage = async () => {
    //     let result = await ImagePicker.launchImageLibraryAsync({
    //         mediaTypes: ImagePicker.MediaTypeOptions.All,
    //         allowsEditing: true,
    //         aspect: [4, 3],
    //         base64: true,
    //     })
    //     if (!result.cancelled) {

    //         var uuid = uuid4();
    //         const fileExtension = result.uri.split('.').pop();
    //         const filename = `${uuid}.${fileExtension}`;
    //         setUrlImage(result.uri)
    //         setImageName(filename)
    //         alert(result.uri)
    //         uploadImage(result.uri)
    //     }
    // }


    const pickImage = async () => {
        try {
            let result = await ImagePicker.launchImageLibraryAsync({
                mediaTypes: ImagePicker.MediaTypeOptions.All,
                allowsEditing: true,
                aspect: [4, 3],
                quality: 1,
                // base64: true,
            })
            if (!result.cancelled) {
                var uuid = uuid4();
                const fileExtension = result.uri.split('.').pop();
                const filename = `${uuid}.${fileExtension}`;
                // setTypeFile(fileExtension)
                setUrlImage(result.uri)
                setImageName(filename)
                uploadImage(result.uri, filename)
                    .then(() => {
                        // Alert.alert("Success");
                        setImage("");
                    })
                console.log(result.uri)
            }

            console.log(result);
        } catch (E) {
            console.log(E);
        }
    };

    const uploadImage = async (uri: any, filename: any) => {
        const response = await fetch(uri);
        const blob = await response.blob();
        // alert("response", response)

        const uploadTask = storage.ref(`images/${filename}`).put(blob);
        uploadTask.on(
            "state_changed",
            snapshot => {
                const progress = Math.round(
                    (snapshot.bytesTransferred / snapshot.totalBytes) * 100
                );
                setProgressBar({ status: true, progress: progress });
                // setProgressBar({ ...progressBar, })
            },
            error => {
                alert(error);
            },
            () => {
                storage
                    .ref("images")
                    .child(filename)
                    .getDownloadURL()
                    .then(url => {
                        setImage(url);
                        setProgressBar({ status: false, progress: 0 });
                        Alert.alert("Success");
                        // setProgressBar({ ...progressBar, status: false })

                    }).catch(err => alert(err));
            }
        );
    }

    // const uploadImage = async (uri, imageName) => {
    //     const response = await fetch(uri);
    //     const blob = await response.blob();

    //     const uploadTask = storage.ref(`images/${imageName}`).put(blob);
    //     uploadTask.on(
    //         "state_changed",
    //         snapshot => {
    //             const progress = Math.round(
    //                 (snapshot.bytesTransferred / snapshot.totalBytes) * 100
    //             );
    //             setProgressBar({ status: true, progress: progress });
    //         },
    //         error => {
    //             // setError(error);
    //             alert(error)
    //         },
    //         () => {
    //             storage
    //                 .ref("images")
    //                 .child(imageName)
    //                 .getDownloadURL()
    //                 .then(url => {
    //                     setImage(url);
    //                     setProgressBar({ status: false, progress: 0 });
    //                 });
    //         }
    //     );
    // }

    const sendMessage = () => {
        if (mgs) {
            socket.emit('sendMessage', { user: userName, type: 'message', data: mgs, fileExtension: 'text', time: new Date() });
            setMge("")
        }
        else if (urlImage) {
            if (progressBar.status == false) {
                if (image) {
                    socket.emit('sendMessage', { user: userName, type: 'file', data: image, fileExtension: 'image', time: new Date() });
                    setUrlImage("");

                }

            }
        }

        // else if (image) {
        //   if (typeFile == 'jpg') {
        //     socket.emit('sendMessage', { user: userRedux.name, type: 'file', data: url, fileExtension: 'image' });
        //     setImage("");

        //   } else if (typeFile == 'mp4') {
        //     socket.emit('sendMessage', { user: userRedux.name, type: 'file', data: url, fileExtension: 'video' });
        //     setImage("");


        //   } else if (typeFile == 'mov') {
        //     socket.emit('sendMessage', { user: userRedux.name, type: 'file', data: url, fileExtension: 'video' });
        //     setImage("");


        //   } else if (typeFile == 'png') {
        //     socket.emit('sendMessage', { user: userRedux.name, type: 'file', data: url, fileExtension: 'image' });
        //     setImage("");

        //   }
        //   return setImage("");
        // }
    }


    // console.log("openBottomSheet", openBottomSheet)

    return (
        <View style={styles.container}>
            {
                openBottomSheet && (
                    <BottomBar openBottomSheet={openBottomSheet} fall={fall} item={route.params.params.item} socket />
                )
            }

            <Animated.View style={{ flex: 1, opacity: Animated.add(0.2, Animated.multiply(fall, 1.0)) }}>
                {/* <KeyboardAvoidingView
                    behavior={Platform.OS === "ios" ? "position" : "position"}
                    keyboardVerticalOffset={50}
                    // style={{ flex: 1 }}
                > */}
                <ScrollView
                    style={{ flex: 1, paddingHorizontal: 16, marginBottom: 16, }}
                    ref={ref => { scrollView = ref }}
                    showsVerticalScrollIndicator={false}
                // onContentSizeChange={() => scrollView.scrollToEnd({ animated: true })}
                >
                    {
                        messages.map((item, index) => (
                            <View style={{ backgroundColor: "transparent", }}>
                                <Messages data={item} userName={userName} />
                            </View>
                        ))
                    }
                </ScrollView>
                {/* </KeyboardAvoidingView> */}


                {
                    urlImage != "" && (
                        <View style={{ height: 100, justifyContent: "center", alignItems: "center", paddingVertical: 8 }}>
                            <View style={{ width: "30%", height: "100%", justifyContent: "center", alignItems: "center", }}>
                                <Image
                                    source={{ uri: urlImage }}
                                    style={{
                                        height: "100%",
                                        width: "100%",
                                    }}
                                />
                            </View>
                            {
                                progressBar.progress == 0 ?
                                    <View></View>
                                    :
                                    <View style={{ justifyContent: "center", alignItems: "center" }}>
                                        <ProgressBar progress={progressBar.progress} indeterminate={true} visible={progressBar.status} color={Colors.red800} style={{ width: 150 }} />
                                        <Text>{progressBar.progress}%</Text>
                                    </View>
                            }
                        </View>
                    )

                }

                <View style={{ height: 55, paddingHorizontal: 14, alignItems: "center", flexDirection: "row", paddingVertical: 9 }}>
                    <TouchableOpacity onPress={() => pickImage()}>
                        <Image
                            source={require("../assets/images/add.png")}
                            style={{
                                height: 24,
                                width: 24,
                            }}
                        />
                    </TouchableOpacity>

                    <View style={{ flex: 1, height: 37, justifyContent: "center", paddingStart: 16 }}>
                        <TextInput
                            value={mgs}
                            onChangeText={(e: any) => {
                                setMge(e)
                            }}
                            placeholder="Aa.."
                            style={{ height: 37, backgroundColor: "#FAFAFA", borderRadius: 50, paddingHorizontal: 16, color: "#0D0D0D" }}

                        />
                    </View>
                    <TouchableOpacity style={{ width: 40, justifyContent: "center", alignItems: "flex-end" }} onPress={() => sendMessage()} >
                        <Image
                            source={require("../assets/images/paper-plane.png")}
                            style={{
                                height: 24,
                                width: 24
                            }}
                        />
                    </TouchableOpacity>

                </View>

            </Animated.View>

        </View >
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: "#FFF6F6",
        // alignItems: 'center',
        // justifyContent: 'center',
        // padding: 20,
    },
    title: {
        fontSize: 20,
        fontWeight: 'bold',
    },
    link: {
        marginTop: 15,
        paddingVertical: 15,
    },
    linkText: {
        fontSize: 14,
        color: '#2e78b7',
    },
});



{/* <View style={{ flexDirection: "row", backgroundColor: "transparent", marginTop: 16 }}>
                        <View style={{ backgroundColor: "transparent", justifyContent: "flex-end" }}>
                            <Avatar rounded title="N" size="small" containerStyle={{ backgroundColor: "#8D8586" }} titleStyle={{ fontSize: 18 }} />
                        </View>

                        <View style={{ backgroundColor: "transparent", marginStart: 16, }}>
                            <View style={{ backgroundColor: "transparent", marginStart: 8 }}>
                                <Text style={{ fontSize: 14, color: "#8D8586" }}>Nong</Text>
                            </View>

                            <View style={{ height: 33, backgroundColor: "#FFFFFF", justifyContent: "center", marginTop: 4, borderRadius: 17, paddingHorizontal: 12, paddingVertical: 7 }}>
                                <Text style={{ fontSize: 14, color: "#8D8586" }}>hello my friend -_-</Text>
                            </View>
                        </View>

                        <View style={{ backgroundColor: "transparent", justifyContent: "flex-end", marginStart: 8 }}>
                            <Text style={{ color: "#8D8586" }}>11:22</Text>
                        </View>
                    </View>

                    <View style={{ marginTop: 16, justifyContent: "flex-end", backgroundColor: "transparent", flexDirection: "row", paddingStart: 57, }}>
                        <View style={{ backgroundColor: "transparent", justifyContent: "flex-end", marginEnd: 8 }}>
                            <Text style={{ color: "#8D8586" }}>11:22</Text>
                        </View>

                        <View style={{ backgroundColor: "#FFE1E2", paddingHorizontal: 12, paddingVertical: 7, borderRadius: 17, maxWidth: "95%", }}>
                            <Text style={{ fontSize: 14, color: "#8D8586" }}>
                                Yap! Nong
                            </Text>
                        </View>
                    </View>

                    <View style={{ flexDirection: "row", backgroundColor: "transparent", marginTop: 16 }}>
                        <View style={{ backgroundColor: "transparent", justifyContent: "flex-end" }}>
                            <Avatar rounded title="K" size="small" containerStyle={{ backgroundColor: "#8D8586" }} titleStyle={{ fontSize: 18 }} />
                        </View>

                        <View style={{ backgroundColor: "transparent", marginStart: 16, maxWidth: "95%", }}>
                            <View style={{ backgroundColor: "transparent", marginStart: 8 }}>
                                <Text style={{ fontSize: 14, color: "#8D8586" }}>Kong</Text>
                            </View>

                            <View style={{ height: 33, backgroundColor: "#FFFFFF", justifyContent: "center", marginTop: 4, borderRadius: 17, paddingHorizontal: 12, paddingVertical: 7, }}>
                                <Text style={{ fontSize: 14, color: "#8D8586" }}>Lorem ipsum dapibus</Text>
                            </View>
                        </View>

                        <View style={{ backgroundColor: "transparent", justifyContent: "flex-end", marginStart: 8 }}>
                            <Text style={{ color: "#8D8586" }}>11:52</Text>
                        </View>
                    </View>

                    <View style={{ marginTop: 16, justifyContent: "flex-end", backgroundColor: "transparent", flexDirection: "row", paddingStart: 57, }}>
                        <View style={{ backgroundColor: "transparent", justifyContent: "flex-end", marginEnd: 8 }}>
                            <Text style={{ color: "#8D8586" }}>11:22</Text>
                        </View>

                        <View style={{ backgroundColor: "#FFE1E2", paddingHorizontal: 12, paddingVertical: 7, borderRadius: 17, maxWidth: "95%", }}>
                            <Text style={{ fontSize: 14, color: "#8D8586" }}>
                                Secondary line text Lorem ipsum dapibus, neque id
                            </Text>
                        </View>
                    </View>

                    <View style={{ marginTop: 16, justifyContent: "flex-end", backgroundColor: "transparent", flexDirection: "row", paddingStart: 28, }}>
                        <View style={{ backgroundColor: "transparent", justifyContent: "flex-end", marginEnd: 8 }}>
                            <Text style={{ color: "#8D8586" }}>11:55</Text>
                        </View>

                        <View style={{ backgroundColor: "#FFE1E2", paddingHorizontal: 12, paddingVertical: 7, borderRadius: 17, maxWidth: "95%", }}>
                            <Text style={{ fontSize: 14, color: "#8D8586" }}>
                                Secondary line text Lorem ipsum dapibus, neque id,Secondary line text Lorem ipsum dapibus, neque id,Secondary line text Lorem ipsum dapibus, neque id
                            </Text>
                        </View>
                    </View>


                    <View style={{ flexDirection: "row", backgroundColor: "transparent", marginTop: 16, paddingEnd: 64 }}>
                        <View style={{ backgroundColor: "transparent", justifyContent: "flex-end" }}>
                            <Avatar rounded title="K" size="small" containerStyle={{ backgroundColor: "#8D8586" }} titleStyle={{ fontSize: 18 }} />
                        </View>

                        <View style={{ backgroundColor: "transparent", marginStart: 16, maxWidth: "95%", }}>
                            <View style={{ backgroundColor: "transparent", marginStart: 8 }}>
                                <Text style={{ fontSize: 14, color: "#8D8586" }}>Kong</Text>
                            </View>

                            <View style={{ backgroundColor: "#FFFFFF", justifyContent: "center", marginTop: 4, borderRadius: 17, paddingHorizontal: 12, paddingVertical: 7, }}>
                                <Text style={{ fontSize: 14, color: "#8D8586" }}>
                                    Lorem ipsum dapibus Lorem ipsum dapibus Lorem ipsum dapibus Lorem ipsum dapibusLorem ipsum dapibus
                                </Text>
                            </View>
                        </View>

                        <View style={{ backgroundColor: "transparent", justifyContent: "flex-end", marginStart: 8 }}>
                            <Text style={{ color: "#8D8586" }}>11:52</Text>
                        </View>
                    </View> */}