import React, { useEffect, useRef, useState } from 'react';
import { Text, View } from './Themed';
import BottomSheet from 'reanimated-bottom-sheet';
import { Image, TouchableOpacity } from 'react-native';
import { MaterialIcons } from '@expo/vector-icons';
import { useDispatch } from 'react-redux';
import { SetBottomSheet } from '../redux/app/appReducer';
import { useNavigation } from '@react-navigation/native';
import Overlay from './Overlay';

export default function BottomBar({ openBottomSheet, fall, item, ...props }: any) {

    const bs = useRef(null);
    const dispatch = useDispatch();
    const navigation = useNavigation();
    const [isVisible, setIsVisible] = useState(false)
    const [Mode, setMode] = useState("")

    const handleCancle = () => {
        // bs.current.snapTo(1)
        dispatch(SetBottomSheet(!openBottomSheet))
    }

    const closeOverlay = () => {
        setIsVisible(false)
    }

    const OnLeftGroup = () => {
        setIsVisible(true);
        setMode("LeftGroup");
        // dispatch(SetBottomSheet(!openBottomSheet))
        // navigation.navigate("Main");
    }


    console.log("isVisible", isVisible)
    const renderInner = () => (
        <View style={{ backgroundColor: "#FFFFFF", height: 300, paddingHorizontal: 16, }}>
            <TouchableOpacity style={{ height: 50, alignItems: "center", flexDirection: "row" }} onPress={() => OnLeftGroup()}>
                <Image
                    source={require("../assets/images/exitMenu.png")}
                    style={{
                        height: 20,
                        width: 20,
                    }}
                />
                <View style={{ flex: 1, marginStart: 16 }}>
                    <Text style={{ fontSize: 16 }}>Left group</Text>
                </View>
            </TouchableOpacity>

            <TouchableOpacity style={{ height: 50, alignItems: "center", flexDirection: "row" }} onPress={() => navigation.navigate("MemberGroup", { screen: 'MemberGroup', params: { item } })}>
                <Image
                    source={require("../assets/images/group.png")}
                    style={{
                        height: 20,
                        width: 20,
                    }}
                />
                <View style={{ flex: 1, marginStart: 16 }}>
                    <Text style={{ fontSize: 16 }}>See Group member</Text>
                </View>
            </TouchableOpacity>

            <TouchableOpacity style={{ height: 50, alignItems: "center", flexDirection: "row" }} onPress={() => handleCancle()}>
                <MaterialIcons name="close" size={20} color="black" />
                <View style={{ flex: 1, marginStart: 16 }}>
                    <Text style={{ fontSize: 16 }}>Cancel</Text>
                </View>
            </TouchableOpacity>

            {
                isVisible && (
                    <Overlay visible={isVisible} closeOverlay={closeOverlay} title={Mode} openBottomSheet />
                )
            }
        </View >

    )
    useEffect(() => {
        if (openBottomSheet == false) {
            bs.current.snapTo(1)
        }
        if (openBottomSheet == true) {
            bs.current.snapTo(0)
        }
    }, [openBottomSheet])

    return (

        <BottomSheet
            ref={bs}
            snapPoints={[150, 0]}
            renderContent={renderInner}
            // renderHeader={renderHeader}
            initialSnap={1}
            callbackNode={fall}
            enabledGestureInteraction={false}
        />
    )
}