import { useNavigation } from '@react-navigation/native';
import React, { useEffect, useState } from 'react';
import { Dimensions, FlatList, Image, StyleSheet, TouchableOpacity, View } from 'react-native';
import { useDispatch, useSelector } from 'react-redux';
import { Text } from './Themed';
import { Avatar, ButtonGroup } from 'react-native-elements';
import { AntDesign } from '@expo/vector-icons';
import { Item } from 'react-native-paper/lib/typescript/src/components/List/List';
import Overlay from './Overlay';
import { RootState } from '../redux';
import { io } from 'socket.io-client';
import SocketApi from '../utils/Socket';


const { width, height } = Dimensions.get("window");

const Data = [{ id: 1, name: "Nong", status: "Admin" },
{ id: 2, name: "Kong", status: "Member" },
{ id: 3, name: "Bee", status: "Member" },
{ id: 4, name: "Cat", status: "Member" },
{ id: 5, name: "Antd", status: "Member" },
]


let socket: any;
let EndPoint = "http://10.80.14.192:9000";

export default function MemberGroup({ route }: any) {

    const navigation = useNavigation();
    const dispatch = useDispatch();

    const [index, setIndex] = useState(0)
    const [isVisible, setIsVisible] = useState(false)
    const [Mode, setMode] = useState("")


    const listUser = useSelector((state: RootState) => state.room.ListUser || Data)
    const userName = useSelector((state: RootState) => state.auth.username)
    const adminUser = route.params.params.item.admin

    const component1 = () => <Text style={{ color: index == 0 ? "#FFFFFF" : "#F27579" }}>All</Text>
    const component2 = () => <Text style={{ color: index == 1 ? "#FFFFFF" : "#F27579" }}>Admin</Text>

    const buttons = [{ element: component1 }, { element: component2 }]
    const updateIndex = (value: any) => {
        setIndex(value)
    }

    const closeOverlay = () => {
        setIsVisible(false)
    }

    useEffect(() => {

        SocketApi.connect(EndPoint);

        return () => {
            SocketApi.disconnect()
        }
    }, [EndPoint])

    const handlerKick = () => {
        setIsVisible(true);
        setMode("OnKick");
        SocketApi.emit('disconnected', { 'date': new Date() });
    }

    // console.log("ListUser", listUser)
    useEffect(() => {

    }, [])


    return (
        <View style={styles.container}>
            <View style={{ justifyContent: "center", alignItems: "center", paddingTop: 24 }}>
                <ButtonGroup
                    onPress={updateIndex}
                    selectedIndex={index}
                    buttons={buttons}
                    containerStyle={{ height: 36, width: 164 }}
                    selectedButtonStyle={{ backgroundColor: "#F27579" }}
                />
            </View>

            <View style={{ marginTop: 24, paddingHorizontal: 24 }}>
                <FlatList
                    // data={index == 0 ? Data : Data.filter((v) => v.status == "Admin")}
                    data={index == 0 ? listUser : listUser.filter((v) => v.name == adminUser)}
                    renderItem={({ item, index }) => (
                        <View style={{ marginBottom: 8, borderBottomColor: "#E4E4E4", borderBottomWidth: 1 }}>
                            <View style={{ flexDirection: "row", marginBottom: 8 }}>
                                <View style={{ justifyContent: "center" }}>
                                    <Avatar rounded title={item.name.substring(0, 1)} size="small" containerStyle={{ backgroundColor: "#8D8586" }} titleStyle={{ fontSize: 18 }} />
                                </View>

                                <View style={{ marginStart: 8, justifyContent: "center" }}>
                                    <View style={{ marginBottom: 4 }}>
                                        <Text style={{ fontSize: 18 }}>
                                            {item.name}
                                        </Text>
                                    </View>

                                    <View style={{ justifyContent: "center" }}>
                                        <Text style={{ color: "#C7C7C7" }}>
                                            {item.name == adminUser ? "Admin" : "Member"}
                                        </Text>
                                    </View>
                                </View>
                                {
                                    (item.name != adminUser && userName == adminUser) && (
                                        < View style={{ flex: 1, justifyContent: "center", alignItems: "flex-end", marginEnd: 8 }}>
                                            <TouchableOpacity style={{ width: 50, alignItems: "flex-end", height: 40, justifyContent: "center" }} onPress={() => handlerKick()}>
                                                <AntDesign name="closecircleo" size={24} color="#F27579" />
                                            </TouchableOpacity>
                                        </View>
                                    )

                                }
                            </View>
                        </View>
                    )}
                    keyExtractor={item => item.name}
                />
            </View>

            {
                isVisible && (
                    <Overlay visible={isVisible} closeOverlay={closeOverlay} title={Mode} />
                )
            }
        </View>
    );
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#FAFAFA',
        // alignItems: 'center',
        // justifyContent: 'center',
        // padding: 20,
    },
    title: {
        fontSize: 20,
        fontWeight: 'bold',
    },
    link: {
        marginTop: 15,
        paddingVertical: 15,
    },
    linkText: {
        fontSize: 14,
        color: '#2e78b7',
    },
});
