import { useNavigation } from '@react-navigation/native';
import { StackScreenProps } from '@react-navigation/stack';
import * as React from 'react';
import { Alert, Image, StyleSheet, Text, TouchableOpacity, View } from 'react-native';
import { Avatar } from 'react-native-elements';
import { Card, Searchbar } from 'react-native-paper';
import { useDispatch, useSelector } from 'react-redux';
import { rootRef } from '../../firebase/firebase';
import { RootState } from '../../redux';
import { logout } from '../../redux/auth/authReducer';
import Overlay from '../Overlay';

export default function Profile() {
    const navigation = useNavigation();
    const dispatch = useDispatch();

    const [isVisible, setIsVisible] = React.useState(false)
    const [roomMode, setRoomMode] = React.useState("")

    const userName = useSelector((state: RootState) => state.auth.username)
    const token = useSelector((state: RootState) => state.auth.token)
    const keyData = useSelector((state: RootState) => state.auth.keyData)
    const closeOverlay = () => {
        setIsVisible(false)
    }

    const onLogout = () => {

        Alert.alert(
            "Are you sure?",
            "Are you sure to log out?",
            [
                {
                    text: "Cancel",

                    style: "cancel",
                },
                {
                    text: "OK",
                    onPress: () => {
                        dispatch(logout());
                        if(keyData){
                               rootRef.ref('users/' + keyData).remove()
                        }
                        navigation.navigate("Login")
                    },
                },
            ],
            { cancelable: false }
        );
    }

    // console.log("username", userName)
    console.log("token", token)

    return (
        <View style={styles.container}>
            <View style={{ height: 250, backgroundColor: "#F27579", paddingVertical: 16, justifyContent: "center", alignItems: "center" }}>
                <Avatar rounded title={userName.substring(0, 2)} size="xlarge" containerStyle={{ backgroundColor: "#FCB3B5", borderColor: "#FFFFFF", borderWidth: 3 }} />
                <View style={{ marginTop: 14 }}>
                    <Text style={{ color: "#FFFFFF", fontSize: 18 }}>{userName}</Text>
                </View>

            </View>

            <View style={{ paddingHorizontal: 28, paddingTop: 28, alignItems: "center" }}>
                <Card style={{ width: "100%", marginBottom: 8, backgroundColor: "#FFFFFF", paddingVertical: 12, paddingHorizontal: 16, elevation: 3, maxWidth: 500 }}>
                    <TouchableOpacity style={{ flexDirection: "row", }} onPress={() => { setIsVisible(true), setRoomMode("CreateRoom") }}>
                        <View style={{ justifyContent: "center", alignItems: "center" }}>
                            <Image
                                source={require("../../assets/images/CreateRoom.png")}
                                style={{
                                    height: 26,
                                    width: 26,
                                    marginRight: 16
                                }}
                            />
                        </View>
                        <View>
                            <Text style={{ color: "#8D8586", fontSize: 21, fontFamily: "Condensed-Italic" }}>Create new room</Text>
                        </View>
                    </TouchableOpacity>
                </Card>

                <Card style={{ width: "100%", marginBottom: 8, backgroundColor: "#FFFFFF", paddingVertical: 12, paddingHorizontal: 16, elevation: 3, maxWidth: 500 }}>
                    <TouchableOpacity style={{ flexDirection: "row", }} onPress={() => onLogout()}>
                        <View style={{ justifyContent: "center", alignItems: "center" }}>
                            <Image
                                source={require("../../assets/images/exit.png")}
                                style={{
                                    height: 24,
                                    width: 24,
                                    marginRight: 16
                                }}
                            />
                        </View>
                        <View>
                            <Text style={{ color: "#8D8586", fontSize: 21, fontFamily: "Condensed-Italic" }}>Logout</Text>
                        </View>
                    </TouchableOpacity>
                </Card>
            </View>
            {
                isVisible && (
                    <Overlay visible={isVisible} closeOverlay={closeOverlay} title={roomMode} />
                )
            }
        </View>
    );
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#FAFAFA',
        // backgroundColor: '#fff',
        // alignItems: 'center',
        // justifyContent: 'center',
        // padding: 20,
    },
    title: {
        fontSize: 20,
        fontWeight: 'bold',
    },
    link: {
        marginTop: 15,
        paddingVertical: 15,
    },
    linkText: {
        fontSize: 14,
        color: '#2e78b7',
    },
});
