import { useNavigation } from '@react-navigation/native';
import React, { useEffect, useState } from 'react';
import { Dimensions, FlatList, Image, StyleSheet, TouchableOpacity, View } from 'react-native';
import { Searchbar } from 'react-native-paper';
import { useDispatch, useSelector } from 'react-redux';
import Database from '../../firebase/roomListType';
import { SetBottomSheet } from '../../redux/app/appReducer';
import { SetRooms } from '../../redux/room/roomReducer';

import { getRoomState } from '../../redux/room/selector';
import Button from '../Button';
import Overlay from '../Overlay';
import { Text } from '../Themed';
import moment from 'moment';


const { width, height } = Dimensions.get("window");

const Data = [
    { id: 1, nameRoom: "FootBall", status: "public", password: "" },
    { id: 2, nameRoom: "Party", status: "private", password: "1234" },
    { id: 3, nameRoom: "Game", status: "public", password: "" }
]

export default function ChatRoom() {
    const navigation = useNavigation();
    const dispatch = useDispatch();

    const [searchQuery, setSearchQuery] = useState("");
    const [isVisible, setIsVisible] = useState(false)
    const [roomMode, setRoomMode] = useState("")

    const [roomData, setRoomData] = useState({})

    const onChangeSearch = (query: any) => setSearchQuery(query);
    const listRooms = useSelector(getRoomState);

    // console.log("listRooms", listRooms)

    const normalRoom = ({ item, index }: any) => {
        setIsVisible(true);
        setRoomMode("Normal");
        navigation.navigate("ChatScreen", { screen: 'ChatScreen', params: { item, index } });
    }

    const privateRoom = ({ item, index }: any) => {
        setIsVisible(true)
        setRoomMode("Lock")
        // console.log("roomData", item)
        setRoomData(item)
    }

    const closeOverlay = () => {
        setIsVisible(false)
    }

    useEffect(() => {
        dispatch(SetBottomSheet(false))
    }, [])

    useEffect(() => {
        Database.onRooms((listRooms: any[]) => {
            dispatch(SetRooms(listRooms))
        });
    }, [])


    var filterData = searchQuery == "" ?
        listRooms : listRooms.filter((v) => v.nameRoom.match(searchQuery))


    return (
        <View style={styles.container}>
            <View style={{ height: 60, backgroundColor: "#F27579", paddingHorizontal: 21, paddingVertical: 10 }}>
                <Searchbar
                    placeholder="Search room..."
                    onChangeText={onChangeSearch}
                    value={searchQuery}
                    style={{ height: 38, color: "#AEACAC" }}
                    inputStyle={{ color: "#AEACAC", fontSize: 14, fontFamily: "Condensed", textDecorationColor: "red" }}
                />
            </View>

            <View style={{ flexDirection: width < 320 ? "column" : "row", marginTop: 26, justifyContent: "center", }}>
                <View style={{ minWidth: 200, maxWidth: 300, backgroundColor: "#F27579", height: 40, justifyContent: "center", }}>
                    <Text style={{ fontSize: 24, color: "#FFFFFF", marginStart: 33, fontFamily: "Condensed-Bold" }}>All Chat Room</Text>
                </View>

                <View style={{ flex: 1, justifyContent: "center", alignItems: width < 320 ? "flex-start" : "flex-end", marginEnd: 16, marginTop: width < 320 ? 8 : 0 }}>
                    <TouchableOpacity style={{ flexDirection: "row", }} onPress={() => { setIsVisible(true), setRoomMode("CreateRoom") }}>
                        <Image
                            source={require("../../assets/images/add_active.png")}
                            style={{
                                height: 20,
                                width: 20,
                                marginRight: 4
                                // resizeMode: "contain",
                            }}
                        />
                        <Text style={{ color: "#F27579", fontFamily: "Condensed-Italic" }}>Create room</Text>
                    </TouchableOpacity>
                </View>
            </View>

            <View style={{ flex: 1, marginHorizontal: 14, marginVertical: 12, alignItems: "center" }}>

                {

                    filterData.length != 0 ?
                        <FlatList
                            style={{ width: "100%", maxWidth: 500, }}
                            data={filterData.sort((a, b) =>moment(new Date(a.timeToCreate)) < moment(new Date(b.timeToCreate)) ? 1 : -1)}
                            renderItem={({ item, index }) => (
                    <View>
                        {
                            item.status == "private" ?
                                <View style={{ flexDirection: "row", backgroundColor: "#FFFFFF", height: 64, marginBottom: 10 }}>
                                    <View style={{ width: 54, backgroundColor: "#FCE28C", justifyContent: "center", alignItems: "center" }}>
                                        <Image
                                            source={require("../../assets/images/padlock.png")}
                                            style={{
                                                height: 24,
                                                width: 24,
                                            }}
                                        />
                                    </View>
                                    <View style={{ flex: width <= 320 ? 2 : 3, justifyContent: "center" }}>
                                        <Text style={{ fontSize: 18, marginStart: 16 }}>{item.nameRoom}</Text>
                                    </View>
                                    <View style={{ flex: 1, justifyContent: "center" }}>
                                        <Button primary title="Join"
                                            onPress={() => privateRoom({ item, index })}
                                            styleText={{
                                                fontFamily: "Condensed-Bold"
                                            }}
                                            style={{
                                                maxWidth: 60,
                                                maxHeight: 40,
                                                fontFamily: "Condensed-Bold"
                                            }} />
                                    </View>
                                </View>
                                :
                                <View style={{ flexDirection: "row", backgroundColor: "#FFFFFF", height: 64, marginBottom: 10 }}>
                                    <View style={{ width: 54, backgroundColor: "#C7C7C7", justifyContent: "center", alignItems: "center" }}>
                                        <Image
                                            source={require("../../assets/images/world.png")}
                                            style={{
                                                height: 24,
                                                width: 24,
                                            }}
                                        />
                                    </View>
                                    <View style={{ flex: width <= 320 ? 2 : 3, justifyContent: "center" }}>
                                        <Text style={{ fontSize: 18, marginStart: 16 }}>{item.nameRoom}</Text>
                                    </View>
                                    <View style={{ flex: 1, justifyContent: "center" }}>
                                        <Button primary title="Join"
                                            onPress={() => normalRoom({ item, index })}
                                            styleText={{
                                                fontFamily: "Condensed-Bold"
                                            }}
                                            style={{
                                                maxWidth: 60,
                                                maxHeight: 40,

                                            }} />
                                    </View>
                                </View>
                        }
                    </View>
                )}
                            keyExtractor={(item) => item.nameRoom}
                        />
                        :
                        <View style={{ flex: 1, justifyContent: "center", alignItems: "center" }}>
                    <Text>No Data</Text>
                </View>
                }

            </View>
            {
                isVisible && (
                    <Overlay visible={isVisible} closeOverlay={closeOverlay} title={roomMode} item={roomData} />
                )
            }
        </View>
    );
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#FAFAFA',
        // alignItems: 'center',
        // justifyContent: 'center',
        // padding: 20,
    },
    title: {
        fontSize: 20,
        fontWeight: 'bold',
    },
    link: {
        marginTop: 15,
        paddingVertical: 15,
    },
    linkText: {
        fontSize: 14,
        color: '#2e78b7',
    },
});
