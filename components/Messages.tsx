import React from 'react';
import { Avatar } from 'react-native-elements';
import { Text, View } from './Themed';
import moment from 'moment';
import { Dimensions, Image } from 'react-native';
// const { width, height } = Dimensions.get("window");

export default function Messages({ data, userName }: any) {
    console.log("data", data)
    return (
        data.user == userName ?
            (
                <View style={{ backgroundColor: "transparent", }}>
                    {
                        data.fileExtension == "image" ?
                            <View style={{ marginTop: 16, justifyContent: "flex-end", backgroundColor: "transparent", flexDirection: "row", paddingStart: 57, }}>
                                <View style={{ backgroundColor: "transparent", justifyContent: "flex-end", marginEnd: 8 }}>
                                    <Text style={{ color: "#8D8586" }}>{moment(data.time).format("H:mm")}</Text>
                                </View>

                                <View style={{ backgroundColor: "#FFE1E2", paddingHorizontal: 12, paddingVertical: 7, borderRadius: 17, width: "100%", height: "100%", maxWidth: 267, maxHeight: 200 }}>
                                    <Image
                                        source={{ uri: data.data }}
                                        style={{
                                            height: "100%",
                                            width: "100%"
                                        }}
                                    />
                                </View>
                            </View>
                            :
                            < View style={{ marginTop: 16, justifyContent: "flex-end", backgroundColor: "transparent", flexDirection: "row", paddingStart: 57, }}>
                                <View style={{ backgroundColor: "transparent", justifyContent: "flex-end", marginEnd: 8 }}>
                                    <Text style={{ color: "#8D8586" }}>{moment(data.time).format("H:mm")}</Text>
                                </View>

                                <View style={{ backgroundColor: "#FFE1E2", paddingHorizontal: 12, paddingVertical: 7, borderRadius: 17, maxWidth: "95%", }}>
                                    <Text style={{ fontSize: 14, color: "#8D8586" }}>
                                        {data.data}
                                    </Text>
                                </View>
                            </View >

                    }
                </View >
            )
            :
            (
                <View style={{ backgroundColor: "transparent", }}>
                    {
                        data.fileExtension == "image" ?
                            <View style={{ flexDirection: "row", backgroundColor: "transparent", marginTop: 16, paddingEnd: 90 }}>
                                <View style={{ backgroundColor: "transparent", justifyContent: "flex-end" }}>
                                    <Avatar rounded title={data.user.substring(0, 1)} size="small" containerStyle={{ backgroundColor: "#8D8586" }} titleStyle={{ fontSize: 18 }} />
                                </View>

                                <View style={{
                                    backgroundColor: "transparent", marginStart: 16,
                                }}>
                                    <View style={{ backgroundColor: "transparent", marginStart: 8 }}>
                                        <Text style={{ fontSize: 14, color: "#8D8586" }}>{data.user}</Text>
                                    </View>

                                    <View style={{
                                        backgroundColor: "#FFFFFF",
                                        justifyContent: "center",
                                        marginTop: 4,
                                        borderRadius: 17,
                                        paddingHorizontal: 12,
                                        paddingVertical: 7,
                                        maxWidth: 267,
                                        maxHeight: 200,
                                        width: "100%",
                                        minHeight: 200,
                                        minWidth: 267,
                                        height: "100%"
                                    }}>
                                        <Image
                                            source={{ uri: data.data }}
                                            style={{
                                                height: "100%",
                                                width: "100%"
                                            }}
                                        />
                                    </View>
                                </View>

                                <View style={{ backgroundColor: "transparent", justifyContent: "flex-end", marginStart: 8 }}>
                                    <Text style={{ color: "#8D8586" }}>{moment(data.time).format("H:mm")}</Text>
                                </View>
                            </View>
                            :
                            <View style={{ flexDirection: "row", backgroundColor: "transparent", marginTop: 16, paddingEnd: 90 }}>
                                <View style={{ backgroundColor: "transparent", justifyContent: "flex-end", height: 55 }}>
                                    <Avatar rounded title={data.user.substring(0, 1)} size="small" containerStyle={{ backgroundColor: "#8D8586" }} titleStyle={{ fontSize: 18 }} />
                                </View>

                                <View style={{ backgroundColor: "transparent", marginStart: 16, }}>
                                    <View style={{ backgroundColor: "transparent", marginStart: 8 }}>
                                        <Text style={{ fontSize: 14, color: "#8D8586" }}>{data.user}</Text>
                                    </View>

                                    <View style={{ backgroundColor: "#FFFFFF", justifyContent: "center", marginTop: 4, borderRadius: 17, paddingHorizontal: 12, paddingVertical: 7 }}>
                                        <Text style={{ fontSize: 14, color: "#8D8586" }}>
                                        dsadjasdaskdjkasdjklsajdlksajdksajdlksajdklsajdklsajdlksajdlksajdlksajdlksajdsakljdlsakjlsadj
                                        dsadjasdaskdjkasdjklsajdlksajdksajdlksajdklsajdklsajdlksajdlksajdlksajdlksajdsakljdlsakjlsadjdsadjasdaskdjkasdjklsajdlksajdksajdlksajdklsajdklsajdlksajdlksajdlksajdlksajdsakljdlsakjlsadj
                                        dsadjasdaskdjkasdjklsajdlksajdksajdlksajdklsajdklsajdlksajdlksajdlksajdlksajdsakljdlsakjlsadj
                                        
                                        </Text>
                                    </View>
                                </View>

                                <View style={{ backgroundColor: "transparent", justifyContent: "flex-end", marginStart: 8 }}>
                                    <Text style={{ color: "#8D8586" }}>{moment(data.time).format("H:mm")}</Text>
                                </View>
                            </View>

                    }

                </View >


            )
    )
}