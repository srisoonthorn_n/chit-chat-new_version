import React from 'react';
import { TextInput as TextInputPaper } from "react-native-paper";
import { TextInput as NativeTextInput } from "react-native";

export default function TextInput({ ...props }) {
    return (
        <TextInputPaper
            mode="flat"
            theme={{
                colors: {
                    primary: "#F27579",
                    text: "#8D8586",
                    placeholder: "#8D8586",
                }
            }}
            // style={{ backgroundColor: "#FFFFFF", fontFamily: "Kanit-Regular" }}
            {...props}
            render={props => <NativeTextInput
                {...props}

                style={{ padding: 6, position: 'relative', color: "#8F8F8F", fontFamily: "Condensed", backgroundColor: "transparent" }}
            />}
        />

    )
}


{/* <TextInputplaceholder='Username'containerStyle={{ maxWidth: 184 }}inputContainerStyle={{ borderBottomColor: "#F27579", borderBottomWidth: 2, }} style={{ maxWidth: 184, width: "100%" }}/> */ }