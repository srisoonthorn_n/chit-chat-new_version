import React from 'react';
import { StyleSheet, Text, TouchableOpacity } from 'react-native';


const Button = ({
    login,
    primary,
    secondary,
    style,
    styleText,
    disabled,
    title,
    ...props
}: any) => {
    const inputStyle = [
        login && styles.login,
        primary && styles.primary,
        secondary && styles.secondary,
        disabled && styles.disabled,
        { justifyContent: "center", alignItems: "center" },
        { ...style, justifyContent: "center", alignItems: "center" },
    ];

    const textStyles = [
        login && styles.loginText,
        primary && styles.primaryText,
        secondary && styles.secondaryText,
        disabled && styles.disabledText,
        { ...styleText }
    ];

    return (
        <TouchableOpacity
            {...(disabled ? null : { ...props })}
            style={inputStyle}
        >
            <Text style={textStyles}>{title}</Text>
        </TouchableOpacity>
    )
}

export default Button

const styles = StyleSheet.create({
    login: {
        borderColor: "#FFFFFF",
        borderRadius: 8,
        borderWidth: 2,
        width: "100%",
        maxWidth: 269,
        padding: 7
    },
    primary: {
        borderColor: "#F27579",
        borderWidth: 2,
        borderRadius: 8,
        padding: 8
    },
    secondary: {
        backgroundColor: "#F27579",
        borderRadius: 8,
        padding: 10,
        // borderColor: "#F27579",
        // borderWidth: 2,
    },
    disabled: {

    },
    loginText: {
        fontSize: 18,
        color: "#FFFFFF",
        fontFamily: "Condensed-Bold"
    },
    primaryText: {
        color: "#F27579",
        fontFamily: "Condensed",
        fontSize: 18,
    },
    secondaryText: {
        color: "#FFFFFF",
        fontFamily: "Condensed",
        fontSize: 18,
    },
    disabledText: {

    }

})