import { FontAwesome5 } from '@expo/vector-icons';
import React, { useState } from 'react';
import { TouchableOpacity } from 'react-native';
import { CheckBox as CheckBoxNative } from "react-native-elements";
import { Text, View } from './Themed';

const CheckBox = ({
    title,
    color,
    handleNecessary,
    defaultCheck,
    keyData,
    ...props
}: any) => {
    const [isCheck, setisCheck] = useState(defaultCheck || false);

    return (
        <TouchableOpacity
            activeOpacity={0.7}
            onPress={() => {
                handleNecessary(!isCheck, keyData);
                setisCheck(!isCheck);
            }}
            style={{
                flexDirection: "row",
                zIndex: 1,
                height: 60,
                alignItems: "center",
                justifyContent: "flex-start",
                ...props
            }}
        >
            <View style={{ flexDirection: "row", justifyContent: "center", alignItems: "center", marginRight: 5, zIndex: 2 }}>
                <CheckBoxNative
                    checkedIcon={<FontAwesome5 name="check-square" size={24} solid color="#F27579" />}
                    uncheckedIcon={<FontAwesome5 name="square" size={24} color="#F27579" />}
                    checkedColor={"#F27579"}
                    checked={isCheck}
                    onPress={() => {
                        handleNecessary(!isCheck, keyData);
                        setisCheck(!isCheck);
                    }}
                />
                <Text style={{ fontSize: 16, color: "#8D8586" }}>{title}</Text>
            </View>
        </TouchableOpacity>
    )
}

export default CheckBox


