import React from "react";
import { StyleSheet, Text, View } from "react-native";

export default function CheckBoxLabel({ title, style, ...props }: any) {
    const inputStyle = [styles.input, { ...style }];

    return (
        <View style={styles.container}>
            <Text style={inputStyle}>
                {title}
            </Text>
        </View>
    );
}

const styles = StyleSheet.create({
    container: {
        height: 60,
        justifyContent: "center"
    },
    input: {
        fontSize: 16,
        fontWeight: "500",
        color: "#707070",
        fontFamily: 'Kanit-Regular'

    }
});
