import React, { useState } from 'react';
import { Dimensions, Image, StyleSheet, TouchableOpacity } from 'react-native';
import { Text, View } from './Themed';
import { Overlay as Popup } from "react-native-elements";
import Button from './Button';
import TextInput from './TextInput';
import CheckBox from './CheckBox';
import { rootRef } from '../firebase/firebase';
import { useDispatch, useSelector } from 'react-redux';
import { RootState } from '../redux';
import Database from '../firebase/roomListType';
import { Rooms } from '../redux/room/types';
import uuid4 from 'react-native-uuid'
import { addNewRoom } from '../redux/room/roomReducer';
import { useNavigation } from '@react-navigation/native';
import moment from 'moment';
import { SetBottomSheet } from '../redux/app/appReducer';

const { width, height } = Dimensions.get("window");

export default function Overlay({ visible, closeOverlay, title, item, ...props }: any) {

    const dispatch = useDispatch();
    const navigation = useNavigation();

    const userName = useSelector((state: RootState) => state.auth.username)

    const [password, setPassword] = useState("");
    const [passwordRoomCheck, setPasswordRoomCheck] = useState(false);
    const [nameRoom, setNameRoom] = useState("");
    const [passwordRoom, setPasswordRoom] = useState("");

    const [errormsg, setErrorMsg] = useState("");
    const [isError, setIsError] = useState(false)
    const timeToCreate = moment()

    // console.log("props", item)
    const handlePasswordRoom = () => {
        setPasswordRoomCheck(!passwordRoomCheck);
        setPasswordRoom("")
    }

    const handleInputPassword = () => {
        if (password == item.password) {
            closeOverlay()
            setIsError(false)
            setErrorMsg("")
            navigation.navigate("ChatScreen", { screen: 'ChatScreen', params: { item } })
        } else {
            // alert("Incorate")
            setIsError(true)
            setErrorMsg("Password incorrect.")
        }
    }

    const handleCrateRoom = async () => {
        var uuid = uuid4();
        const roomData: Rooms = {
            key: "",
            id: uuid,
            nameRoom: nameRoom,
            password: passwordRoom,
            admin: userName,
            status: passwordRoomCheck == true ? "private" : "public",
            timeToCreate: timeToCreate.toString(),
        }
        dispatch(addNewRoom(roomData));
        Database.addNewCase(roomData).then(res => {
            closeOverlay()
        }).catch(err => {
            alert(JSON.stringify(err))
        })

        // await rootRef.ref().child('rooms/').push({ nameRoom: nameRoom, passwordRoom: passwordRoom, user: userName })
        //     .then(() => {
        //         closeOverlay()
        //     })
        //     .catch((error: any) => {
        //         console.log(error)
        //     });

    }

    const onLeftGroup = () => {
        closeOverlay()
        dispatch(SetBottomSheet(!props.openBottomSheet));
        navigation.navigate("Main");
    }

    return (
        <Popup isVisible={visible} onBackdropPress={closeOverlay}>
            <View style={styles.container}>
                {
                    (title == "Lock") && (
                        <View style={{ justifyContent: "center", paddingBottom: 24, backgroundColor: "transparent", }}>
                            <View style={{ justifyContent: "center", marginEnd: 10, alignItems: "flex-end", marginBottom: 8, backgroundColor: "transparent", }}>
                                <TouchableOpacity style={{ width: 50, height: 30, alignItems: "flex-end", justifyContent: "center", }}
                                    onPress={closeOverlay}>
                                    <Image
                                        source={require("../assets/images/close.png")}
                                        style={{
                                            height: 15,
                                            width: 15,
                                        }}
                                    />
                                </TouchableOpacity>
                            </View>
                            <View style={{ justifyContent: "center", alignItems: "center", backgroundColor: "transparent", }}>
                                <Image
                                    source={require("../assets/images/private.png")}
                                    style={{
                                        height: 90,
                                        width: 90,
                                    }}
                                />
                                <Text style={{ color: "#F27579", fontFamily: "Condensed-Bold", marginVertical: 8, fontSize: 18 }}>Private room</Text>
                                <Text style={{ color: "#C7C7C7", fontSize: 16 }}>Please input  password room.</Text>
                            </View>

                            <View style={{ paddingHorizontal: 32, marginTop: 19, backgroundColor: "transparent", }}>
                                <Text style={{ color: password == "" ? "#8D8586" : "#F27579", marginStart: 6 }}>Password *</Text>
                                <TextInput
                                    error={isError}
                                    placeholder='Password room'
                                    value={password}
                                    secureTextEntry
                                    style={{ height: 10, backgroundColor: "#FFFFFF" }}
                                    onChangeText={(e: any) => {
                                        setPassword(e)
                                    }}
                                />
                                {
                                    isError && (
                                        <Text style={{ color: "red", fontSize: 14, marginTop: 8 }}>{errormsg}</Text>
                                    )
                                }

                            </View>

                            <View style={{ flexDirection: "row", marginTop: 36, backgroundColor: "transparent", }}>
                                <View style={{ flex: 1, backgroundColor: "transparent", }}>
                                    <Button
                                        primary
                                        title="Cancle"
                                        onPress={closeOverlay}
                                        styleText={{
                                            fontFamily: "Condensed-Italic"
                                        }}
                                    />
                                </View>
                                <View style={{ flex: 1, marginStart: 8, backgroundColor: "transparent", }}>
                                    <Button
                                        secondary
                                        title="OK"
                                        onPress={() => handleInputPassword()}
                                        styleText={{
                                            fontFamily: "Condensed-Italic"
                                        }}
                                    />
                                </View>

                            </View>
                        </View>
                    )
                }
                {
                    (title == "Normal") && (
                        <View style={{ justifyContent: "center", paddingBottom: 24 }}>
                            <View style={{ justifyContent: "center", marginEnd: 10, alignItems: "flex-end", marginBottom: 8, }}>
                                <TouchableOpacity style={{ width: 50, height: 30, alignItems: "flex-end", justifyContent: "center", }}
                                    onPress={closeOverlay}>
                                    <Image
                                        source={require("../assets/images/close.png")}
                                        style={{
                                            height: 15,
                                            width: 15,
                                        }}
                                    />
                                </TouchableOpacity>
                            </View>
                            <View style={{ justifyContent: "center", alignItems: "center" }}>
                                <Image
                                    source={require("../assets/images/check.png")}
                                    style={{
                                        height: 90,
                                        width: 90,
                                    }}
                                />
                                <Text style={{ color: "#F27579", fontFamily: "Condensed-Bold", marginVertical: 8, fontSize: 18 }}>Successful</Text>
                                <Text style={{ color: "#C7C7C7", fontSize: 16 }}>you have successfully to join group.</Text>
                            </View>

                            <View style={{ flexDirection: "row", marginTop: 30, justifyContent: "center", alignItems: "center" }}>
                                <View style={{ width: 200, }}>
                                    <Button
                                        secondary
                                        onPress={closeOverlay}
                                        title="OK"
                                        styleText={{
                                            fontFamily: "Condensed-Italic"
                                        }} />
                                </View>

                            </View>
                        </View>
                    )
                }
                {
                    (title == "CreateRoom") && (
                        <View style={{ justifyContent: "center", paddingBottom: 24 }}>
                            <View style={{ justifyContent: "center", marginEnd: 10, alignItems: "flex-end", marginBottom: 8, }}>
                                <TouchableOpacity style={{ width: 50, height: 30, alignItems: "flex-end", justifyContent: "center", }}
                                    onPress={closeOverlay}>
                                    <Image
                                        source={require("../assets/images/close.png")}
                                        style={{
                                            height: 15,
                                            width: 15,
                                        }}
                                    />
                                </TouchableOpacity>
                            </View>
                            <View style={{ justifyContent: "center", alignItems: "center" }}>
                                <Image
                                    source={require("../assets/images/CreateRoom.png")}
                                    style={{
                                        height: 90,
                                        width: 90,
                                    }}
                                />
                                <View style={{
                                    borderLeftWidth: 5,
                                    borderRightWidth: 5,
                                    borderTopWidth: 1,
                                    borderBottomWidth: 1,
                                    marginTop: 24,
                                    marginBottom: 16,
                                    paddingHorizontal: 8,
                                    borderColor: "#F27579",

                                }}>
                                    <Text style={{
                                        color: "#F27579",
                                        fontFamily: "Condensed-Bold",
                                        fontSize: 18
                                    }}>
                                        Create new room
                                </Text>
                                </View>
                            </View>

                            <View style={{ marginHorizontal: 22 }}>
                                <View style={{ marginBottom: 8 }}>
                                    <Text style={{ marginStart: 6, color: nameRoom ? "#F27579" : "#8D8586" }}>Name room *</Text>
                                    <TextInput
                                        placeholder='Input name room'
                                        value={nameRoom}
                                        style={{ height: 10, backgroundColor: "#FFFFFF" }}
                                        onChangeText={(e: any) => {
                                            setNameRoom(e)
                                        }}
                                    />
                                </View>

                                <View>
                                    {
                                        passwordRoomCheck ?
                                            <Text style={{ marginStart: 6, color: passwordRoom ? "#F27579" : "#8D8586" }}>Password</Text>
                                            :
                                            <Text style={{ marginStart: 6, color: "#BEC2C9" }}>Password</Text>
                                    }

                                    <TextInput
                                        disabled={passwordRoomCheck ? false : true}
                                        placeholder='Input password room'
                                        value={passwordRoom}
                                        secureTextEntry
                                        style={{ height: 10, backgroundColor: "#FFFFFF" }}
                                        onChangeText={(e: any) => {
                                            setPasswordRoom(e)
                                        }}
                                    />
                                </View>
                            </View>

                            <View style={{ justifyContent: "center", width: 120, marginHorizontal: 8 }}>
                                <CheckBox title="Private room" defaultCheck={passwordRoomCheck} handleNecessary={handlePasswordRoom} />
                            </View>



                            <View style={{ flexDirection: "row", marginTop: 36, justifyContent: "center" }}>
                                <View style={{ width: 90, }}>
                                    <Button
                                        primary
                                        title="Cancle"
                                        onPress={closeOverlay}
                                        styleText={{
                                            fontFamily: "Condensed-Italic"
                                        }}
                                    />
                                </View>
                                <View style={{ width: 90, marginStart: 8 }}>
                                    <Button
                                        secondary
                                        onPress={() => handleCrateRoom()}
                                        title="OK"
                                        styleText={{
                                            fontFamily: "Condensed-Italic"
                                        }}

                                    />
                                </View>

                            </View>
                        </View>
                    )
                }
                {
                    (title == "LeftGroup") && (
                        <View style={{ justifyContent: "center", paddingBottom: 24 }}>
                            <View style={{ justifyContent: "center", marginEnd: 10, alignItems: "flex-end", marginBottom: 8, }}>
                                <TouchableOpacity style={{ width: 50, height: 30, alignItems: "flex-end", justifyContent: "center", }}
                                    onPress={closeOverlay}>
                                    <Image
                                        source={require("../assets/images/close.png")}
                                        style={{
                                            height: 15,
                                            width: 15,
                                        }}
                                    />
                                </TouchableOpacity>
                            </View>
                            <View style={{ justifyContent: "center", alignItems: "center" }}>
                                <Image
                                    source={require("../assets/images/check.png")}
                                    style={{
                                        height: 90,
                                        width: 90,
                                    }}
                                />
                                <Text style={{ color: "#F27579", fontFamily: "Condensed-Bold", marginVertical: 8, fontSize: 18 }}>Successful</Text>
                                <Text style={{ color: "#C7C7C7", fontSize: 16 }}>you have successfully to Left group.</Text>
                            </View>

                            <View style={{ flexDirection: "row", marginTop: 30, justifyContent: "center", alignItems: "center" }}>
                                <View style={{ width: 200, }}>
                                    <Button
                                        secondary
                                        onPress={() => onLeftGroup()}
                                        title="OK"
                                        styleText={{
                                            fontFamily: "Condensed-Italic"
                                        }} />
                                </View>

                            </View>
                        </View>
                    )
                }
                {
                    (title == "OnKick") && (
                        <View style={{ justifyContent: "center", paddingBottom: 24 }}>
                            <View style={{ justifyContent: "center", marginEnd: 10, alignItems: "flex-end", marginBottom: 8, }}>
                                <TouchableOpacity style={{ width: 50, height: 30, alignItems: "flex-end", justifyContent: "center", }}
                                    onPress={closeOverlay}>
                                    <Image
                                        source={require("../assets/images/close.png")}
                                        style={{
                                            height: 15,
                                            width: 15,
                                        }}
                                    />
                                </TouchableOpacity>
                            </View>
                            <View style={{ justifyContent: "center", alignItems: "center" }}>
                                <Image
                                    source={require("../assets/images/check.png")}
                                    style={{
                                        height: 90,
                                        width: 90,
                                    }}
                                />
                                <Text style={{ color: "#F27579", fontFamily: "Condensed-Bold", marginVertical: 8, fontSize: 18 }}>Successful</Text>
                                <Text style={{ color: "#C7C7C7", fontSize: 16 }}>you have successfully to kick member.</Text>
                            </View>

                            <View style={{ flexDirection: "row", marginTop: 30, justifyContent: "center", alignItems: "center" }}>
                                <View style={{ width: 90, }}>
                                    <Button
                                        primary
                                        title="Cancle"
                                        onPress={closeOverlay}
                                        styleText={{
                                            fontFamily: "Condensed-Italic"
                                        }}
                                    />
                                </View>
                                <View style={{ width: 90, marginStart: 8 }}>
                                    <Button
                                        secondary
                                        onPress={() => { alert("Kick"), closeOverlay() }}
                                        title="OK"
                                        styleText={{
                                            fontFamily: "Condensed-Italic"
                                        }}

                                    />
                                </View>

                            </View>
                        </View>
                    )
                }
            </View>

        </Popup >

    )
}

const styles = StyleSheet.create({
    container: {
        // paddingBottom: 20,
        width: width - 150,
        maxHeight: height - 100,
        backgroundColor: "transparent",
    }
})