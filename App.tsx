import React, { useState } from 'react';
import { SafeAreaProvider } from 'react-native-safe-area-context';
import useCachedResources from './hooks/useCachedResources';
import useColorScheme from './hooks/useColorScheme';
import Navigation from './navigation';
import * as Font from "expo-font";
import { AppLoading } from "expo";
import { StatusBar } from 'react-native';
import { Provider } from "react-redux";
import { persistor, store } from "./configuationStore";
import { PersistGate } from 'redux-persist/integration/react';
import { Provider as PaperProvider } from 'react-native-paper';

const fetchFont = () => {
  return Font.loadAsync({
    "Kanit-Regular": require("./assets/fonts/Kanit-Regular.ttf"),
    "Kanit-Bold": require("./assets/fonts/Kanit-Bold.ttf"),
    "Kanit-Italic": require("./assets/fonts/Kanit-Italic.ttf"),
    "Condensed": require("./assets/fonts/BarlowCondensed-Regular.otf"),
    "Condensed-Bold": require("./assets/fonts/BarlowCondensed-Bold.otf"),
    "Condensed-Italic": require("./assets/fonts/BarlowCondensed-Italic.otf"),

  });
};


export default function App() {
  const [fontLoaded, setFontLoaded] = useState(false);
  const isLoadingComplete = useCachedResources();
  const colorScheme = useColorScheme();

  if (!fontLoaded) {
    return (
      <AppLoading
        startAsync={fetchFont}
        onFinish={() => {
          setFontLoaded(true);
        }}
      />
    );
  }

  if (!isLoadingComplete) {
    return null;
  } else {
    return (
      <Provider store={store}>
        <PersistGate loading={null} persistor={persistor}>
          <PaperProvider>
            <SafeAreaProvider>
              <Navigation colorScheme={colorScheme} />
              <StatusBar />
            </SafeAreaProvider>
          </PaperProvider>
        </PersistGate>
      </Provider>
    );
  }
}
