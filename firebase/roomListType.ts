import { Rooms } from "../redux/room/types";
import { rootRef } from "./firebase";
import firebase from 'firebase';




class DatabaseClass {
  RoomsRef: firebase.database.Reference;
  constructor() {
    this.RoomsRef = rootRef.ref("rooms");
  }

  async addNewCase(item: Rooms) {
    let ref = await this.RoomsRef.push(JSON.parse(JSON.stringify(item)));
    let updatedData = {
      ...item,
      key: ref.key + "",
    };
    await this.updateCase(updatedData);
    return updatedData;
  }

  async updateCase(item: Rooms) {
    return this.RoomsRef
      .child(item.key)
      .update(JSON.parse(JSON.stringify(item)));
  }

  onRooms(callback : any) {
    return this.RoomsRef.on("value", snapshot => {
      let rooms: any[] = [];
      snapshot.forEach(data => {
        if (data.val()) {
          rooms.push(data.val());
        }
      });
      //   alert(">>>>" + JSON.stringify(cases.length));
      callback(rooms);
    });
  }
}

const Database = new DatabaseClass();
export default Database;
