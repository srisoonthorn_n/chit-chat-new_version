export type RootStackParamList = {
  Login: undefined;
  Main: undefined;
  ChatScreen: undefined;
  MemberGroup: undefined;
  NotFound: undefined;

};

export type BottomTabParamList = {
  ChatRoom: undefined;
  Profile: undefined;
};

export type ChatRoomParamList = {
  ChatRoom: undefined;
}

export type ProfileParamList = {
  Profile: undefined;
}

export type ChatScreenParamList = {
  ChatScreen: undefined;
}

export type DrawerParamList = {
  ChatScreen: undefined;
}
// export type TabOneParamList = {
//   TabOneScreen: undefined;
// };

// export type TabTwoParamList = {
//   TabTwoScreen: undefined;
// };
