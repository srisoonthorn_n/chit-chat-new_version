import { Ionicons } from '@expo/vector-icons';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { useNavigation } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import * as React from 'react';
import { Image, TouchableOpacity } from 'react-native';
import { useSelector } from 'react-redux';
import ChatRoom from '../components/Main/ChatRoom';
import Profile from '../components/Main/Profile';
import { Text, View } from '../components/Themed';

import Colors from '../constants/Colors';
import useColorScheme from '../hooks/useColorScheme';
import { RootState } from '../redux';
import { BottomTabParamList, ChatRoomParamList, ProfileParamList, } from '../types';

const BottomTab = createBottomTabNavigator<BottomTabParamList>();

export default function BottomTabNavigator() {
  const colorScheme = useColorScheme();
  const navigation = useNavigation();
  const currentTab = useSelector((state: RootState) => state.app.currentActiveBottomTab)

  return (
    <BottomTab.Navigator
      initialRouteName="ChatRoom"
    // tabBarOptions={{ activeTintColor: "#F27579" }}
    >
      <BottomTab.Screen
        name="ChatRoom"
        component={ChatRoomNavigator}
        options={{
          tabBarButton: props =>
            <View style={{ flex: 1, justifyContent: "center", }}>

              <TouchableOpacity style={{ flex: 1, justifyContent: "center" }} onPress={() => navigation.navigate("ChatRoom")}>
                {
                  currentTab == "ChatRoom" ?
                    <View style={{ justifyContent: "center", alignItems: "center" }}>
                      <Image
                        source={require("../assets/images/chat_(1).png")}
                        style={{ width: 20, height: 20 }}
                      />
                      <Text style={{ fontSize: 12, color: "#F27579" }}>Chat Room</Text>
                    </View>

                    :
                    <View style={{ justifyContent: "center", alignItems: "center" }}>
                      < Image
                        source={require("../assets/images/chat.png")}
                        style={{ width: 20, height: 20 }}
                      />
                      <Text style={{ fontSize: 12, color: "#8D8586" }}>Chat Room</Text>
                    </View>



                }

              </TouchableOpacity>
            </View>
        }}
      />
      {/* {_ =>
          <View style={{ justifyContent: "center", backgroundColor: "#FFFFFF" }}>
            <ChatRoom />
          </View>}
      </BottomTab.Screen> */}

      <BottomTab.Screen
        name="Profile"
        component={ProfileNavigator}
        options={{
          tabBarButton: props =>
            <View style={{ flex: 1, justifyContent: "center", }}>

              <TouchableOpacity style={{ flex: 1, justifyContent: "center" }} onPress={() => navigation.navigate("Profile")}>
                {
                  currentTab == "Profile" ?
                    <View style={{ justifyContent: "center", alignItems: "center" }}>
                      <Image
                        source={require("../assets/images/user.png")}
                        style={{ width: 20, height: 20 }}
                      />
                      <Text style={{ fontSize: 12, color: "#F27579" }}>Profile</Text>
                    </View>
                    :
                    <View style={{ justifyContent: "center", alignItems: "center" }}>
                      < Image
                        source={require("../assets/images/user_(1).png")}
                        style={{ width: 20, height: 20 }}
                      />
                      <Text style={{ fontSize: 12, color: "#8D8586" }}>Profile</Text>
                    </View>



                }

              </TouchableOpacity>
            </View>
        }}
      />
      {/* {_ =>
          <View style={{ justifyContent: "center", backgroundColor: "#FFFFFF" }}>
            <Profile />
          </View>}
      </BottomTab.Screen> */}
    </BottomTab.Navigator>
  );
}

// You can explore the built-in icon families and icons on the web at:
// https://icons.expo.fyi/
function TabBarIcon(props: { name: string; color: string }) {
  return <Ionicons size={30} style={{ marginBottom: -3 }} {...props} />;
}

// Each tab has its own navigation stack, you can read more about this pattern here:
// https://reactnavigation.org/docs/tab-based-navigation#a-stack-navigator-for-each-tab

const ChatRoomStack = createStackNavigator<ChatRoomParamList>();

function ChatRoomNavigator() {
  return (
    <ChatRoomStack.Navigator>
      <ChatRoomStack.Screen
        name="ChatRoom"
        component={ChatRoom}
        options={{
          headerStyle: {
            backgroundColor: "#F27579",
            height: 70,
            borderBottomWidth: 0,
            borderBottomColor: "transparent",
          },
          headerLeft: () => <View style={{ flex: 1, flexDirection: "row", marginRight: 8, }} />,
          headerTitle: () => (
            <View
              style={{
                flex: 1,
                justifyContent: "center",
                backgroundColor: "#F27579",
                alignItems: "center",

              }}
            >
              <Image
                source={require("../assets/images/chitchat_logo.png")}
                style={{
                  width: 68,
                  height: 68,
                  resizeMode: "cover",
                }}
              />
            </View>
          ),
          headerRight: () => <View style={{ flex: 1, flexDirection: "row", marginRight: 8, }} />
        }}
      />
    </ChatRoomStack.Navigator>
  )
}

const ProfileStack = createStackNavigator<ProfileParamList>();

function ProfileNavigator() {
  return (
    <ProfileStack.Navigator>
      <ProfileStack.Screen
        name="Profile"
        component={Profile}
        options={{
          headerStyle: {
            backgroundColor: "#F27579",
            height: 70,
            borderBottomWidth: 0,
            shadowColor: "transparent",
            borderBottomColor: "transparent"
          },
          headerLeft: () => <View style={{ flex: 1, flexDirection: "row", marginRight: 8, }} />,
          headerTitle: () => (
            <View
              style={{
                flex: 1,
                justifyContent: "center",
                backgroundColor: "#F27579",
                alignItems: "center",

              }}
            >
              <Image
                source={require("../assets/images/chitchat_logo.png")}
                style={{
                  width: 68,
                  height: 68,
                  resizeMode: "cover",
                }}
              />
            </View>
          ),
          headerRight: () => <View style={{ flex: 1, flexDirection: "row", marginRight: 8, }} />
        }}
      />
    </ProfileStack.Navigator>
  )
}

// const TabOneStack = createStackNavigator<TabOneParamList>();

// function TabOneNavigator() {
//   return (
//     <TabOneStack.Navigator>
//       <TabOneStack.Screen
//         name="ChatRoom"
//         component={ChatRoom}
//         options={{ headerTitle: 'Tab One Title' }}
//       />
//     </TabOneStack.Navigator>
//   );
// }

// const TabTwoStack = createStackNavigator<TabTwoParamList>();

// function TabTwoNavigator() {
//   return (
//     <TabTwoStack.Navigator>
//       <TabTwoStack.Screen
//         name="Profile"
//         component={Profile}
//         options={{ headerTitle: 'Tab Two Title' }}
//       />
//     </TabTwoStack.Navigator>
//   );
// }
