import { NavigationContainer, DefaultTheme, DarkTheme, DrawerActions } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import * as React from 'react';
import { ColorSchemeName, Image, TouchableOpacity } from 'react-native';
import LoginScreen from '../screens/LoginScreen';
import NotFoundScreen from '../screens/NotFoundScreen';
import { DrawerParamList, RootStackParamList } from '../types';
import BottomTabNavigator from './BottomTabNavigator';
import LinkingConfiguration from './LinkingConfiguration';
import { useDispatch, useSelector } from "react-redux";
import { SetBottomSheet, SetCurrentActiveBottomTab } from '../redux/app/appReducer';
import ChatScreen from '../screens/ChatSreen';
import { Feather, Ionicons, MaterialCommunityIcons } from '@expo/vector-icons';
import { Text, View } from '../components/Themed';
import { Entypo } from '@expo/vector-icons';
import { createDrawerNavigator, DrawerContentScrollView, DrawerItem } from '@react-navigation/drawer';
import BottomSheet from 'react-native-js-bottom-sheet'
import { RootState } from '../redux';
import MemberGroup from '../components/MemberGroup';
import SocketApi from '../utils/Socket';

// import BottomDrawer from 'rn-bottom-drawer';
// If you are not familiar with React Navigation, we recommend going through the
// "Fundamentals" guide: https://reactnavigation.org/docs/getting-started
export default function Navigation({ colorScheme }: { colorScheme: ColorSchemeName }) {
  const dispatch = useDispatch();
  return (
    <NavigationContainer
      onStateChange={(rootState) => {
        if (rootState?.routes[rootState.index].state) {

          var navigationState = rootState.routes[rootState.index].state

          if (navigationState?.routes.length) {
            const currentIndex = navigationState.index || 0
            const routeNames = navigationState?.routes || []
            // console.log(">>>>>", routeNames[currentIndex].name)
            dispatch(SetCurrentActiveBottomTab(routeNames[currentIndex].name))
          }
        }
      }}
      // linking={LinkingConfiguration}
      theme={colorScheme === "dark" ? DarkTheme : DefaultTheme}
    >
      <RootNavigator />
    </NavigationContainer>
  );
}

// A root stack navigator is often used for displaying modals on top of all other content
// Read more here: https://reactnavigation.org/docs/modal
const Stack = createStackNavigator<RootStackParamList>();


let EndPoint = "http://10.80.12.209:9000";

function RootNavigator() {

  const [openOverlay, setOpenOverlay] = React.useState(false);
  const openBottomSheet = useSelector((state: RootState) => state.app.openBottomSheet)

  // React.useEffect(() => {
  //   SocketApi.connect(EndPoint)
  //   return () => {
  //     SocketApi.disconnect()
  //   }
  // }, [EndPoint])


  const dispatch = useDispatch();
  const _onPressButton = () => {
    bottomSheet.open()
  }
  const bs = React.useRef(null);
  return (
    <Stack.Navigator screenOptions={{ headerShown: true }}>
      <Stack.Screen
        name="Login"
        component={LoginScreen}
        options={({ navigation, route }) => ({
          header: () => null,
        })}
      />
      <Stack.Screen name="Main" component={BottomTabNavigator}
        options={({ navigation, route }) => ({
          header: () => null,
        })} />
      <Stack.Screen name="ChatScreen" component={ChatScreen}
        options={({ navigation, route }) =>
          ChatScreenHeader(navigation, openBottomSheet, dispatch, bs, route)
        }
      />
      <Stack.Screen name="MemberGroup" component={MemberGroup}
        options={({ navigation, route }) =>
          MemberGroupHeader(navigation)
        }
      />
      <Stack.Screen name="NotFound" component={NotFoundScreen} options={{ title: 'Oops!' }} />
    </Stack.Navigator>
  );
}


// const Drawer = createDrawerNavigator<DrawerParamList>();

// const MenuList = (props) => {
//   return (
//     <DrawerContentScrollView>
//       <DrawerItem
//         label="See Group Member"
//         style={{ borderBottomWidth: 1, borderBottomColor: "#707070", }}
//         icon={() => <MaterialCommunityIcons name="logout-variant" size={24} color="#707070" />}
//         onPress={() => props.navigation.navigate("MemberList")} />

//       <DrawerItem
//         label="Leave Group"
//         style={{ borderBottomWidth: 1, borderBottomColor: "#707070" }}
//         icon={() => <Feather name="users" size={24} color="#707070" />}
//         onPress={() => props.navigation.navigate("Main")} />
//     </DrawerContentScrollView>
//   )
// }

// const DrawerContent = (props) => {
//   return (
//     MenuList(props)
//   )
// }
// function DrawerNavigator() {
//   return (
//     <Drawer.Navigator initialRouteName="ChatScreen" drawerPosition={"right"} screenOptions={{ gestureEnabled: true }} drawerContent={props =>
//       <DrawerContent {...props} />}>
//       <Drawer.Screen name="ChatScreen" component={ChatScreen} />
//     </Drawer.Navigator>
//   )
// }



var bottomSheet: BottomSheet



const ChatScreenHeader = (navigation: any, openBottomSheet: any, dispatch: any, bs: any, route: any) => ({
  headerStyle: {
    backgroundColor: "#F27579",
  },
  headerLeft: () => (
    <TouchableOpacity
      onPress={() => {
        navigation.navigate("Main")
        dispatch(SetBottomSheet(false))
      }

      }
      style={{ marginLeft: 16, width: 80, height: 50, justifyContent: "center" }}
    >
      <Ionicons name="ios-arrow-back" size={24} color="#FFFFFF" />
    </TouchableOpacity>
  ),
  headerTitle: () => (
    <View
      style={{
        flex: 1,
        justifyContent: "center",
        backgroundColor: "transparent",
        alignItems: "center",
      }}
    >
      <Text style={{ fontSize: 20, color: "#FFFFFF" }}>{route.params.params.item.nameRoom}</Text>
    </View>
  ),
  headerRight: () => (
    <TouchableOpacity
      onPress={() => { dispatch(SetBottomSheet(!openBottomSheet)) }}
      style={{ marginRight: 16, width: 80, height: 50, justifyContent: "center", alignItems: "flex-end" }}
    >
      <Entypo name="menu" size={24} color="#FFFFFF" />
    </TouchableOpacity >
  ),
});


const MemberGroupHeader = (navigation: any) => ({
  headerStyle: {
    backgroundColor: "#F27579",
  },
  headerLeft: () => (
    <TouchableOpacity
      onPress={() => {
        navigation.goBack()
      }

      }
      style={{ marginLeft: 16, width: 80, height: 50, justifyContent: "center" }}
    >
      <Ionicons name="ios-arrow-back" size={24} color="#FFFFFF" />
    </TouchableOpacity>
  ),
  headerTitle: () => (
    <View
      style={{
        flex: 1,
        justifyContent: "center",
        backgroundColor: "transparent",
        alignItems: "center",
      }}
    >
      <Text style={{ fontSize: 20, color: "#FFFFFF" }}>Member</Text>
    </View>
  ),
  headerRight: () => (
    <View />
  ),
});
